package sistema;

import dominio.*;
import dominio.Arbol.*;
import dominio.Grafo.*;
import interfaz.*;

public class ImplementacionSistema implements Sistema {
    ArbolBinario<Pasajero> arbolPasajeros;
    ArbolBinario<Pasajero> arbolPasajerosPlatino;
    ArbolBinario<Pasajero> arbolPasajerosFrecuente;
    ArbolBinario<Pasajero> arbolPasajerosEstandar;

    ArbolBinarioMaximo<Aerolinea> arbolAerolineas;
    Grafo<Aeropuerto> grafoAeropuerto;

    
    @Override
    public Retorno inicializarSistema(int maxAeropuertos, int maxAerolineas) {
        if(maxAerolineas <= 3){
            return Retorno.error2("Error: maxAerolineas es menor o igual a 3");
        }
        if(maxAeropuertos <= 5){
            return Retorno.error1("Error: maxAeropuertos es menor o igual a 5");
        }
        arbolPasajeros = new ArbolBinario<>();
        arbolPasajerosPlatino= new ArbolBinario<>();
        arbolPasajerosFrecuente= new ArbolBinario<>();
        arbolPasajerosEstandar= new ArbolBinario<>();

        arbolAerolineas = new ArbolBinarioMaximo<>(maxAerolineas);
        grafoAeropuerto = new Grafo<>(maxAeropuertos);


        return Retorno.ok();
    }

    @Override
    public Retorno registrarPasajero(String cedula, String nombre, String telefono, Categoria categoria) {
        if(cedula == null || cedula.isEmpty() || nombre == null || nombre.isEmpty() || telefono == null || telefono.isEmpty() || categoria == null){
            return Retorno.error1("Error: alguno de los parametros es nulo o vacio");
        }

        if(!Pasajero.isValidCedula(cedula)){
            return Retorno.error2("Error: cedula invalida");
        }

        Pasajero pasajero = new Pasajero(cedula, nombre, telefono, categoria);

        if(!arbolPasajeros.insertar(pasajero)){
            return Retorno.error3("Error: pasajero ya registrado");
        }

        switch (categoria) {
            case PLATINO:
                arbolPasajerosPlatino.insertar(pasajero);
                break;
            case FRECUENTE:
                arbolPasajerosFrecuente.insertar(pasajero);
                break;
            case ESTANDAR:
                arbolPasajerosEstandar.insertar(pasajero);
                break;
            default:
                return Retorno.error1("Error: categoria invalida");
        }

        return Retorno.ok();
    }

    @Override
    public Retorno buscarPasajero(String cedula) {
        if(cedula == null || cedula.isEmpty()){
            return Retorno.error1("Error: cedula es nula o vacia");
        }
        if(!Pasajero.isValidCedula(cedula)){
            return Retorno.error2("Error: cedula invalida");
        }
        Pasajero pasajero = new Pasajero(cedula);

        ResultadoBusqueda resultadoBusqueda = arbolPasajeros.buscar(pasajero);
        
        if(resultadoBusqueda.getElemento() == null){
            return Retorno.error3("Error: pasajero no encontrado");
        }
        
        System.out.println(resultadoBusqueda.getElemento().toString());
        System.out.println(resultadoBusqueda.getContador());
        
        return Retorno.ok(resultadoBusqueda.getContador(),resultadoBusqueda.getElemento().toString());
    }

    @Override
    public Retorno listarPasajerosAscendente() {
        String lista = arbolPasajeros.listarAscendente();
        return Retorno.ok(lista);
    }

    @Override
    public Retorno listarPasajerosPorCategoria(Categoria categoria) {
        if(categoria == null){
            return Retorno.error1("Error: categoria es nula");
        }
        String lista;
        switch (categoria) {
            case PLATINO:
                lista = arbolPasajerosPlatino.listarAscendente();
                break;
            case FRECUENTE:
                lista = arbolPasajerosFrecuente.listarAscendente();
                break;
            case ESTANDAR:
                lista = arbolPasajerosEstandar.listarAscendente();
                break;
            default:
                return Retorno.error1("Error: categoria invalida");
        }
        return Retorno.ok(lista);
    }

    @Override
    public Retorno registrarAerolinea(String codigo, String nombre) {
        if(arbolAerolineas.getCantidadElementos() >= arbolAerolineas.getMaxElementos()){
            return Retorno.error1("Error: cantidad de aerolineas excedida");
        }

        if(codigo == null || codigo.isEmpty() || nombre == null || nombre.isEmpty()){
            return Retorno.error2("Error: alguno de los parametros es nulo o vacio");
        }

        Aerolinea aerolinea = new Aerolinea(nombre, codigo);

        if(!arbolAerolineas.insertar(aerolinea)){
            return Retorno.error3("Error: aerolinea ya registrada");
        }

        return Retorno.ok();
    }

    @Override
    public Retorno listarAerolineasDescendente() {
        String lista = arbolAerolineas.listarDescendente();
        return Retorno.ok(lista);    
    }

    @Override
    public Retorno registrarAeropuerto(String codigo, String nombre) {
        if (!grafoAeropuerto.hayLugar()) {
            return Retorno.error1("Error: cantidad de aeropuertos excedida");
        }

        if (codigo == null || codigo.isEmpty() || nombre == null || nombre.isEmpty()) {
            return Retorno.error2("Error: alguno de los parametros es nulo o vacio");
        }

        Aeropuerto nuevoAeropuerto = new Aeropuerto(codigo, nombre);
        if (grafoAeropuerto.existeVertice(nuevoAeropuerto)) {
            return Retorno.error3("Error: aeropuerto ya registrado");
        }

        grafoAeropuerto.agregarVertice(nuevoAeropuerto);
        return Retorno.ok();
    }

    @Override
    public Retorno registrarConexion(String codigoAeropuertoOrigen, String codigoAeropuertoDestino, double kilometros) {
        if (kilometros <= 0) {
            return Retorno.error1("Error: kilómetros es menor o igual a 0");
        }

        if (codigoAeropuertoOrigen == null || codigoAeropuertoOrigen.isEmpty() ||
                codigoAeropuertoDestino == null || codigoAeropuertoDestino.isEmpty()) {
            return Retorno.error2("Error: alguno de los parametros es nulo o vacio");
        }

        Aeropuerto aeropuertoOrigen = new Aeropuerto(codigoAeropuertoOrigen, "");
        Aeropuerto aeropuertoDestino = new Aeropuerto(codigoAeropuertoDestino,"");

        if (!grafoAeropuerto.existeVertice(aeropuertoOrigen)) {
            return Retorno.error3("Error: no existe el aeropuerto de origen");
        }

        if (!grafoAeropuerto.existeVertice(aeropuertoDestino)) {
            return Retorno.error4("Error: no existe el aeropuerto de destino");
        }

        if (grafoAeropuerto.obtenerArista(aeropuertoOrigen, aeropuertoDestino) != null) {
            return Retorno.error5("Error: ya existe una conexión entre el origen y el destino");
        }

        grafoAeropuerto.agregarArista(aeropuertoOrigen, aeropuertoDestino, kilometros);
        return Retorno.ok();
    }
    @Override
    public Retorno registrarVuelo(String codigoAeropuertoOrigen, String codigoAeropuertoDestino, String codigoDeVuelo, double combustible, double minutos, double costoEnDolares, String codigoAerolinea) {
        // Verificar que los parámetros double sean mayores a 0
        if (combustible <= 0 || minutos <= 0 || costoEnDolares <= 0) {
            return Retorno.error1("Error: alguno de los parámetros double es menor o igual a 0");
        }

        // Verificar que los parámetros String no sean nulos o vacíos
        if (codigoAeropuertoOrigen == null || codigoAeropuertoOrigen.isEmpty() ||
                codigoAeropuertoDestino == null || codigoAeropuertoDestino.isEmpty() ||
                codigoDeVuelo == null || codigoDeVuelo.isEmpty() ||
                codigoAerolinea == null || codigoAerolinea.isEmpty()) {
            return Retorno.error2("Error: alguno de los parámetros String es vacío o null");
        }

        // Crear objetos Aeropuerto para el origen y destino
        Aeropuerto aeropuertoOrigen = new Aeropuerto(codigoAeropuertoOrigen, "");
        Aeropuerto aeropuertoDestino = new Aeropuerto(codigoAeropuertoDestino, "");

        // Verificar que el aeropuerto de origen exista
        if (!grafoAeropuerto.existeVertice(aeropuertoOrigen)) {
            return Retorno.error3("Error: no existe el aeropuerto de origen");
        }

        // Verificar que el aeropuerto de destino exista
        if (!grafoAeropuerto.existeVertice(aeropuertoDestino)) {
            return Retorno.error4("Error: no existe el aeropuerto de destino");
        }

        // Verificar que la aerolínea exista
        if (!arbolAerolineas.existe(new Aerolinea("",codigoAerolinea))) {
            return Retorno.error5("Error: no existe la aerolínea indicada");
        }

        // Verificar que exista una conexión entre el origen y el destino
        Arista<Vuelo> conexion = grafoAeropuerto.obtenerArista(aeropuertoOrigen, aeropuertoDestino);
        if (conexion == null) {
            return Retorno.error6("Error: no existe una conexión entre origen y destino");
        }

        // Verificar que no exista ya un vuelo con el mismo código en esa conexión
        Vuelo nuevoVuelo = new Vuelo(codigoDeVuelo, combustible, minutos, costoEnDolares, codigoAerolinea);
        if (!conexion.agregarElemento(nuevoVuelo)) {
            return Retorno.error7("Error: ya existe un vuelo con ese código en esa conexión");
        }

        // Registrar el vuelo
        conexion.agregarElemento(nuevoVuelo);
        return Retorno.ok();
    }

    @Override
    public Retorno listadoAeropuertosCantDeEscalas(String codigoAeropuertoOrigen, int cantidad, String codigoAerolinea) {
        if (cantidad < 0) {
            return Retorno.error1("Error: la cantidad de escalas es menor que cero");
        }

        Aeropuerto aeropuertoOrigen = new Aeropuerto(codigoAeropuertoOrigen, "");
        if (!grafoAeropuerto.existeVertice(aeropuertoOrigen)) {
            return Retorno.error2("Error: el aeropuerto no está registrado en el sistema");
        }

        Aerolinea aerolinea = new Aerolinea("", codigoAerolinea);
        if (!arbolAerolineas.existe(aerolinea)) {
            return Retorno.error3("Error: la aerolínea no está registrada en el sistema");
        }

        String respuesta = grafoAeropuerto.bfs(aeropuertoOrigen, cantidad, codigoAerolinea);
        return Retorno.ok(respuesta);
    }
    @Override
    public Retorno viajeCostoMinimoKilometros(String codigoCiudadOrigen, String codigoCiudadDestino) {
        return Retorno.noImplementada();
    }

    @Override
    public Retorno viajeCostoMinimoEnMinutos(String codigoAeropuertoOrigen, String codigoAeropuertoDestino) {
        return Retorno.noImplementada();
    }


}
