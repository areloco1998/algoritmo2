package dominio;

public class Tuplas<T extends Comparable<T>> implements Comparable<Tuplas<T>> {
    private int nivel;
    private T elemento;

    public Tuplas(int nivel, T elemento) {
        this.nivel = nivel;
        this.elemento = elemento;
    }

    public int getNivel() {
        return nivel;
    }

    public T getElemento() {
        return elemento;
    }

    @Override
    public int compareTo(Tuplas<T> otraTupla) {
        return this.elemento.compareTo(otraTupla.getElemento());
    }

    @Override
    public String toString() {
        return elemento.toString();
    }
}