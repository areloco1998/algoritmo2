package dominio.Arbol;

public class ArbolBinarioMaximo<T extends Comparable<T>> extends ArbolBinario<T>{

    private int cantidadElementos;
    private int maxElementos;

    public ArbolBinarioMaximo(int cantidad) {
        super();
        this.cantidadElementos = 0;
        this.maxElementos = cantidad;
    }

    
    public int getCantidadElementos() {
        return this.cantidadElementos;
    }

    public int getMaxElementos() {
        return this.maxElementos;
    }

    @Override
    public boolean insertar(T elemento) {
        if (getRaiz() == null) {
            setRaiz(new NodoArbol<>(elemento));
            this.cantidadElementos++;
            return true;
        } else {
            return insertarRecursivo(getRaiz(), elemento);
        }
    }

    private boolean insertarRecursivo(NodoArbol<T> nodoActual, T elemento) {
        if (elemento.equals(nodoActual.getElemento())) {
            return false;
        }

        if (elemento.compareTo(nodoActual.getElemento()) < 0) {
            if (nodoActual.getHijoIzquierdo() == null) {
                nodoActual.setHijoIzquierdo(new NodoArbol<>(elemento));
                this.cantidadElementos++;
                return true;
            } else {
                return insertarRecursivo(nodoActual.getHijoIzquierdo(), elemento);
            }
        } else if (elemento.compareTo(nodoActual.getElemento()) > 0) {
            if (nodoActual.getHijoDerecho() == null) {
                nodoActual.setHijoDerecho(new NodoArbol<>(elemento));
                this.cantidadElementos++;
                return true;
            } else {
                return insertarRecursivo(nodoActual.getHijoDerecho(), elemento);
            }
        }

        return false;
    }

    public boolean existe(T elemento) {
        return buscar(elemento).getElemento() != null;
    }

}
