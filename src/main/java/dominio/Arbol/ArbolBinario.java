package dominio.Arbol;


import dominio.Pasajero;
import dominio.ResultadoBusqueda;
import interfaz.Categoria;

public class ArbolBinario <T extends Comparable<T>> {
    private NodoArbol<T> raiz;

    public ArbolBinario() {
        this.raiz = null;
    }

    public NodoArbol<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoArbol<T> nuevo) {
        this.raiz = nuevo;
    }

    public boolean insertar(T elemento) {
        if (raiz == null) {
            raiz = new NodoArbol<>(elemento);
            return true;
        } else {
            return insertarRecursivo(raiz, elemento);
        }
    }

    private boolean insertarRecursivo(NodoArbol<T> nodoActual, T elemento) {
        if (elemento.equals(nodoActual.getElemento())) {
            return false;
        }

        if (elemento.compareTo(nodoActual.getElemento()) < 0) {
            if (nodoActual.getHijoIzquierdo() == null) {
                nodoActual.setHijoIzquierdo(new NodoArbol<>(elemento));
                return true;
            } else {
                return insertarRecursivo(nodoActual.getHijoIzquierdo(), elemento);
            }
        } else if (elemento.compareTo(nodoActual.getElemento()) > 0) {
            if (nodoActual.getHijoDerecho() == null) {
                nodoActual.setHijoDerecho(new NodoArbol<>(elemento));
                return true;
            } else {
                return insertarRecursivo(nodoActual.getHijoDerecho(), elemento);
            }
        }

        return false;
    }

    public ResultadoBusqueda<T> buscar(T objeto) {
        return buscarRecursivo(raiz, objeto, 0);
    }
    
    private ResultadoBusqueda<T> buscarRecursivo(NodoArbol<T> nodoActual, T pasajero, int contador) {
        if (nodoActual == null) {
            return new ResultadoBusqueda<>(null, contador);
        }
        if (pasajero.equals(nodoActual.getElemento())) {
            return new ResultadoBusqueda<>(nodoActual.getElemento(), contador);
        }
        if (pasajero.compareTo(nodoActual.getElemento()) < 0) {
            return buscarRecursivo(nodoActual.getHijoIzquierdo(), pasajero, contador + 1);
        } else {
            return buscarRecursivo(nodoActual.getHijoDerecho(), pasajero, contador + 1);
        }
    }

    public String listarAscendente() {
        StringBuilder resultado = new StringBuilder();
        listarAscendente(raiz, resultado);
        if (!resultado.isEmpty()) {
            resultado.setLength(resultado.length() - 1);
        }
        return resultado.toString();
    }

    private void listarAscendente(NodoArbol<T> nodoActual, StringBuilder resultado) {
        if (nodoActual != null) {
            listarAscendente(nodoActual.getHijoIzquierdo(), resultado);
            resultado.append(nodoActual.getElemento().toString()).append("|");
            listarAscendente(nodoActual.getHijoDerecho(), resultado);
        }
    }


    public String listarDescendente(){
        String response = listarDescendente(raiz); 
        if(response.isEmpty()) return response;
        return response.substring(0, response.length() - 1);
    }

    private String listarDescendente(NodoArbol<T> nodoActual) {
        String resultado = "";
        if (nodoActual != null) {
            resultado += listarDescendente(nodoActual.getHijoDerecho());
            resultado += nodoActual.getElemento().toString() + "|";
            resultado += listarDescendente(nodoActual.getHijoIzquierdo());
        }
        return resultado;
    }

}