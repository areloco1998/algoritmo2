package dominio;

import interfaz.Categoria;

import java.util.Objects;
import java.util.regex.Pattern;

public class Pasajero implements Comparable<Pasajero> {
    private static String CEDULA_REGEX = "[1-9]\\d{0,2}(\\.\\d{3}){2}-\\d";

    private String cedula;
    private String nombre;
    private String telefono;
    private Categoria categoria;

    private int numeroCedula;

    public Pasajero(String cedula) {
        this.numeroCedula = formatearCedula(cedula);
        this.cedula = cedula;
    }

    public Pasajero(String cedula, String nombre, String telefono, Categoria categoria) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
        this.categoria = categoria;
        this.numeroCedula = formatearCedula(cedula);
    }
    private static int formatearCedula(String cedula) {
        String sinPuntosYGuion = cedula.replaceAll("[.-]", "");
        String soloNumeros = sinPuntosYGuion.substring(0, sinPuntosYGuion.length() - 1);
        return Integer.parseInt(soloNumeros);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pasajero pasajero = (Pasajero) o;
        return Objects.equals(this.cedula, pasajero.cedula);
    }

    static public boolean isValidCedula(String cedula) {
        return Pattern.matches(CEDULA_REGEX, cedula);
    }

    public Categoria getCategoria() {
        return categoria;
    }

    @Override
    public int compareTo(Pasajero t) {
        return Integer.compare(this.numeroCedula, t.numeroCedula);
    }

    @Override
    public String toString() {
        return this.cedula + ";" + this.nombre + ";" + this.telefono + ";" + this.categoria.getTexto();
    }
}
