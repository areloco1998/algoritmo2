package dominio;

import java.util.Objects;

public class Aerolinea implements Comparable<Aerolinea>{

    private String nombre;
    private String codigo;

    public Aerolinea(String nombre, String codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    @Override
    public int compareTo(Aerolinea aerolinea) {
        return this.codigo.compareTo(aerolinea.codigo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aerolinea that = (Aerolinea) o;
        return Objects.equals(codigo, that.codigo);
    }
    @Override
    public String toString() {
        return  this.codigo + ";" + this.nombre;
    }
    
}
