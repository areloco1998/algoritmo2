package dominio;
public class Vuelo {
    private String codigoDeVuelo;
    private double combustible;
    private double minutos;
    private double costoEnDolares;
    private String codigoAerolinea;

    public Vuelo(String codigoDeVuelo, double combustible, double minutos, double costoEnDolares, String codigoAerolinea) {
        this.codigoDeVuelo = codigoDeVuelo;
        this.combustible = combustible;
        this.minutos = minutos;
        this.costoEnDolares = costoEnDolares;
        this.codigoAerolinea = codigoAerolinea;
    }

    public String getCodigoDeVuelo() {
        return codigoDeVuelo;
    }
    public String getCodigoAerolinea() {
        return codigoAerolinea;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Vuelo vuelo = (Vuelo) obj;
        return codigoDeVuelo.equals(vuelo.codigoDeVuelo);
    }

}