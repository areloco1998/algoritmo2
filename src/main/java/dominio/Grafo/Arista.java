package dominio.Grafo;
public class Arista<T> {
    public double kilometros;
    public Lista<T> elementos;

    public Arista(double kilometros) {
        this.kilometros = kilometros;
        this.elementos = new Lista<>();
    }

    public double getKilometros() {
        return kilometros;
    }

    public Lista<T> getElementos() {
        return elementos;
    }

    public void setKilometros(int kilometros) {
        this.kilometros = kilometros;
    }



    public boolean agregarElemento(T elemento) {
        if (elementos.existe(elemento)) {
            return false;
        }
        elementos.insertar(elemento);
        return true;
    }
}
