package dominio.Grafo;

import java.util.Objects;

public class Vertice<T> {
    private T dato;

    public Vertice(T dato) {
        this.dato = dato;
    }

    public T getDato() {
        return dato;
    }

    public void setNombre(T dato) {
        this.dato = dato;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Vertice<?> vertice = (Vertice<?>) obj;
        return dato.equals(vertice.dato);
    }

    @Override
    public String toString() {
        return "Vertice{" +
                "nombre='" + dato + '\'' +
                '}';
    }
}