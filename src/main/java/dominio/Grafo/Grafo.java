package dominio.Grafo;

import dominio.Aeropuerto;
import dominio.Arbol.ArbolBinario;
import dominio.Tuplas;
import dominio.Vuelo;

import java.util.Iterator;

public class Grafo<T extends Comparable<T>> {
    private Vertice<T>[] vertices;
    private Arista[][] aristas;
    private final int maxVertices;
    int cantidad = 0;

    public Grafo(int maxVertices) {
        this.maxVertices = maxVertices;
        vertices = new Vertice[maxVertices];
        aristas = new Arista[maxVertices][maxVertices];
    }

    public boolean hayLugar() {
        return cantidad < maxVertices;
    }

    public void agregarVertice(T dato) {
        if (cantidad < maxVertices) {
            int posLibre = obtenerPosLibre();
            vertices[posLibre] = new Vertice<>(dato);
            cantidad++;
        }
    }

    public boolean existeVertice(T dato) {
        int posABuscar = buscarPos(new Vertice<>(dato));
        return posABuscar >= 0;
    }

    public void agregarArista(T vInicial, T vFinal, double kilometros) {
        int posVInicial = buscarPos(new Vertice<>(vInicial));
        int posVFinal = buscarPos(new Vertice<>(vFinal));

        aristas[posVInicial][posVFinal] = new Arista<>(kilometros);
    }

    public Arista obtenerArista(T vInicial, T vFinal) {
        int posVInicial = buscarPos(new Vertice<>(vInicial));
        int posVFinal = buscarPos(new Vertice<>(vFinal));

        return aristas[posVInicial][posVFinal];
    }

    private int buscarPos(Vertice<T> v) {
        for (int i = 0; i < vertices.length; i++) {
            if (vertices[i] != null && vertices[i].equals(v)) {
                return i;
            }
        }
        return -1;
    }

    private int obtenerPosLibre() {
        for (int i = 0; i < vertices.length; i++) {
            if (vertices[i] == null) {
                return i;
            }
        }
        return -1;
    }
    public String bfs(T origen, int maxEscalas, String codigoAerolinea) {
        ArbolBinario<Tuplas<T>> aeropuertosVisitados = new ArbolBinario<>();
        int posV = buscarPos(new Vertice<>(origen));
        boolean[] visitados = new boolean[maxVertices];
        Cola<Tuplas<T>> cola = new Cola<>();

        cola.encolar(new Tuplas<>(0, vertices[posV].getDato()));
        visitados[posV] = true;

        while (!cola.esVacia()) {
            Tuplas<T> tupla = cola.desencolar();
            int pos = buscarPos(new Vertice<>(tupla.getElemento()));
            int nivel = tupla.getNivel();

            if (nivel > maxEscalas) {
                continue;
            }

            aeropuertosVisitados.insertar(tupla);

            for (int i = 0; i < maxVertices; i++) {
                if (aristas[pos][i] != null && !visitados[i]) {
                    Lista<Vuelo> vuelos = aristas[pos][i].getElementos();
                    Iterator<Vuelo> iterador = vuelos.iterator();
                    boolean tieneVueloConAerolinea = false;
                    while (iterador.hasNext()) {
                        Vuelo vuelo = iterador.next();
                        if (vuelo.getCodigoAerolinea().equals(codigoAerolinea)) {
                            tieneVueloConAerolinea = true;
                            break;
                        }
                    }
                    if (tieneVueloConAerolinea) {
                        cola.encolar(new Tuplas<>(nivel + 1, vertices[i].getDato()));
                        visitados[i] = true;
                    }
                }
            }
        }

        return aeropuertosVisitados.listarAscendente();
    }
}
