package dominio;

public class ResultadoBusqueda<T> implements Comparable<Pasajero>  {
    private T elemento;
    private int contador;

    public ResultadoBusqueda(T elemento, int contador) {
        this.elemento = elemento;
        this.contador = contador;
    }

    public T getElemento() {
        return elemento;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    @Override
    public int compareTo(Pasajero o) {
        throw new UnsupportedOperationException("Unimplemented method 'compareTo'");
    }
}
