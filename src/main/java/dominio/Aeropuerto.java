package dominio;

import java.util.Objects;

public class Aeropuerto implements Comparable<Aeropuerto>{

    private String nombre;
    private String codigo;

    public Aeropuerto(String codigo, String nombre) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aeropuerto that = (Aeropuerto) o;
        return Objects.equals(codigo, that.codigo);
    }

    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    @Override
    public int compareTo(Aeropuerto aeropuerto) {
        return this.codigo.compareTo(aeropuerto.codigo);
    }

    @Override
    public String toString() {
        return this.codigo + ";" + this.nombre;
    }
}
